#!/usr/bin/env node
var spawn = require('child_process').spawn;
var logger = require('fs').createWriteStream('elb_discover.log');

var args = process.argv.slice(2);
var region = args[0];
var tagKey = args[1];
var tagValue = args[2];

var Runner = function () {
  this.actions = [];
};

Runner.prototype = {
  next: function () {
    var action = this.actions.shift();
    if (action) {
      action.apply(this, arguments);
    } else {
      this.exit(0);
    }
  },
  add: function (fn) {
    this.actions.push(fn);
  },
  start: function () {
    this.next();
  },
  exit: function (status) {
    process.exit(status);
  }
};

var runner = new Runner();

runner.add(function getElbs() {
  var _this = this;
  var elbs = '';
  var error = false;

  var describeLoadBalancers = spawn('aws',
    [
      'elb',
      'describe-load-balancers',
      '--region',
      region
    ]
  );

  describeLoadBalancers.stdout.on('data', function (data) {
    elbs += data;
  });

  describeLoadBalancers.stderr.on('data', function (data) {
    error = true;
    logger.write(data);
  });

  describeLoadBalancers.on('close', function () {
    if (error) {
      _this.exit(1);
    }
    elbs = JSON.parse(elbs);
    _this.next(elbs.LoadBalancerDescriptions);
  });
});

runner.add(function checkTags() {
  var elbs = arguments[0];
  var _this = this;
  var tags = '';
  var error = false;

  var names = extractNames(elbs);


  var args = ['elb', 'describe-tags', '--load-balancer-names'];
  args = args.concat(names);
  args.push('--region');
  args.push(region);

  var describeTags = spawn('aws', args);

  describeTags.stdout.on('data', function (data) {
    tags += data;
  });

  describeTags.stderr.on('data', function (data) {
    error = true;
    logger.write(data);
  });

  describeTags.on('close', function () {
    if (error) {
      _this.exit(1);
    }
    tags = JSON.parse(tags);
    _this.next(elbs, tags.TagDescriptions);
  });
});

runner.add(function selectBalancer() {
  var elbs = arguments[0];
  var tags = arguments[1];

  var balancer;

  var i = tags.length;
  while(i--) {
    balancer = tags[i].LoadBalancerName;
    if (tags[i].Tags.length) {
      var l = tags[i].Tags.length;
      while(l--) {
        var tag = tags[i].Tags[l];
        if (tag.Key === tagKey && tag.Value === tagValue) {
          i = l = 0;
        }
      }
    }
  }
  this.next(elbs, balancer);
});

runner.add(function getDns() {
  var elbs = arguments[0];
  var balancer = arguments[1];

  var i = elbs.length;
  while(i--) {
    var elb = elbs[i];
    if (elb.LoadBalancerName === balancer) {
      i = 0;
      this.next(elb.DNSName);
    }
  }
});

runner.add(function printResult() {
  var dns = arguments[0];
  process.stdout.write(dns);
  process.stdout.write('\n');
  logger.end();
  this.exit(0);
});

runner.start();

process.on('uncaughtException', function uncaughtExceptionHandler(err) {
  logger.write('Process UncaughtException\n');
  logger.write(err);
  logger.end();
  process.exit(1);
});

function extractNames(elbs) {
  return elbs.reduce(function (arr, item) {
    arr.push(item.LoadBalancerName);
    return arr;
  }, []);
}
