#!/usr/bin/env node

/* eslint-disable no-process-exit */
var spawn = require('child_process').spawn;
var logger = require('fs').createWriteStream('ami_prune.log');

var args = process.argv.slice(2);
var REGION = args[0];
var AMI_NAME = args[1];
var BRANCH = args[2];
var AMIS_TO_KEEP = 3;

var Runner = function() {
  this.actions = [];
};

Runner.prototype = {
  next: function() {
    var action = this.actions.shift();
    if (action) {
      action.apply(this, arguments);
    } else {
      this.exit(0);
    }
  },
  add: function(fn) {
    this.actions.push(fn);
  },
  start: function() {
    this.next();
  },
  exit: function(status) {
    process.exit(status);
  }
};

var runner = new Runner();

runner.add(function getAmis() {
  var _this = this;
  var amis = '';
  var error = false;

  var describeImages = spawn('aws', [
    'ec2',
    'describe-images',
    '--owners',
    'self',
    '--region',
    REGION
  ]);

  describeImages.stdout.on('data', function(data) {
    amis += data;
  });

  describeImages.stderr.on('data', function(data) {
    error = true;
    logger.write(data);
  });

  describeImages.on('close', function() {
    if (error) {
      _this.exit(1);
    }
    amis = JSON.parse(amis);
    _this.next(amis.Images);
  });
});

runner.add(function filterByTag(amis) {
  var filteredAmis = amis.filter(function hasAmiRightName(ami) {
    var name = getTag(ami, 'Component');
    var hasRightName = (name === AMI_NAME);
    var branch = getTag(ami, 'Branch');
    var hasRightBranch = (branch === BRANCH);
    return (hasRightName && hasRightBranch);
  });
  this.next(filteredAmis);
});

runner.add(function sortByDate(amis) {
  var amisIndexedByDate = amis.reduce(function indexByDate(index, ami) {
    var timeStamp = getTag(ami, 'Date');
    index[timeStamp] = ami;
    return index;
  }, []);
  var sortedAmis = Object.keys(amisIndexedByDate)
    .map(function parseNumber(nr) {
      return parseInt(nr, 10);
    })
    .sort()
    .map(function getAmiForDate(date) {
      return amisIndexedByDate[date];
    });
  this.next(sortedAmis);
});

runner.add(removeAmis);

runner.add(function printResult(result) {
  process.stdout.write('AMIs were pruned! ' + result.length + ' AMIs left!\n');
});

runner.start();

process.on('uncaughtException', function uncaughtExceptionHandler(err) {
  logger.write('Process UncaughtException\n' + err.stack + '\n');
  logger.end();
  process.exit(1);
});

function getTag(ami, tagName) {
  var tags = ami.Tags || [];
  var tagValue = tags.reduce(function getTagValue(prev, tag) {
    var isRightTag = (tag.Key === tagName);
    return isRightTag ? tag.Value : prev;
  }, undefined);
  return tagValue;
}

function removeAmis(amis) {
  if (amis.length <= AMIS_TO_KEEP) {
    return this.next(amis);
  }
  var amiToRemove = amis.shift();
  var imageId = amiToRemove.ImageId;
  process.stdout.write('Deregistering image ' + imageId + '\n');
  var _this = this;
  var output = '';
  var error = false;

  var deregisterImage = spawn('aws', [
    'ec2',
    'deregister-image',
    '--image-id',
    imageId,
    '--region',
    REGION
  ]);

  deregisterImage.stdout.on('data', function(data) {
    output += data;
  });

  deregisterImage.stderr.on('data', function(data) {
    error = true;
    logger.write(data);
  });

  deregisterImage.on('close', function() {
    if (error) {
      _this.exit(1);
    }
    logger.write(output);
    //Recursively call yourself to remove the next AMI.
    removeAmis(amis);
  });
}
